#!/bin/bash

platform="ios"
echo "* Platform: iOS Procursus"
mkdir bin deb tmp
cd tmp
sudo apt install -y autoconf automake autopoint bison cmake cmake-data curl dirmngr docbook-xml docbook-xsl fakeroot file flex gettext gettext-base git gnupg gnupg-utils gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm libarchive13 libcurl4 libfl-dev libfl2 libksba8 liblocale-gettext-perl liblzo2-2 libnghttp2-14 libpod-parser-perl libpython3.9 librtmp1 libsgmls-perl libssh2-1 libterm-readkey-perl libtextstyle0v5 libtool libusb-1.0-0 libuv1 libxml2 libxml2-utils libyaml-tiny-perl m4 make patch perl pinentry pkg-config po4a python3 python3.9 scdaemon triehash wget build-essential clang clang-11 dsymutil dsymutil-11 ld64 libc++-11-dev libc++-dev libclang-common-11-dev libclang-cpp11 libllvm11 liblto liblto11 libtapi libuuid16 libxar1 odcctools text-cmds
if [[ ! -d /usr/share/SDKs/MacOSX.sdk ]]; then
    curl -LO https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/MacOSX10.15.sdk.tar.xz
    7z x MacOSX10.15.sdk.tar.xz
    sudo tar -xvf MacOSX10.15.sdk.tar -C /usr/share/SDKs
    sudo mv /usr/share/SDKs/MacOSX10.15.sdk /usr/share/SDKs/MacOSX.sdk
fi
git clone --recursive https://github.com/LukeeGD/Procursus
cd Procursus
make xpwn-package MEMO_TARGET=iphoneos-arm64 MEMO_CFVER=1500 || true
cd build_stage/iphoneos-arm64/1500/xpwn/usr/
mkdir bin share
mv dmg hdutil hfsplus ipsw ticket validate xpwntool bin/
cd ../../../../..
make xpwn-package MEMO_TARGET=iphoneos-arm64 MEMO_CFVER=1500
mv build_dist/iphoneos-arm64/1500/*.deb ../../deb
cd ..
cp ../deb/xpwn*.deb .
ar x xpwn*.deb
zstd -d data.tar.zst
tar -xvf data.tar -C .
mv usr/bin/ipsw ../bin/ipsw_ios
mv Procursus ..
cd ..
rm -rf tmp
echo "Done! Build at bin/ipsw_ios"
echo "xpwn deb files at deb/*.deb"
